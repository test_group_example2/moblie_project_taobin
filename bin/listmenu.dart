import 'main_project.dart';

class listmenu {

Map getlistmenu(String x) {
    if (x == '1') {
      return recommended;
    } else if (x == "2") {
      return coffee;
    } else if (x == "3") {
      return tea;
    } else if (x == "4") {
      return milkCocoaCaramel;
    } else if (x == "5") {
      return proteinShake;
    } else if (x == "6") {
      return soda;
    }
    return coffee;
  }

  Map recommended = {
    1: listmenuu().expresso,    2: listmenuu().ginger, 3: listmenuu().blackCoffee,
  };
  Map coffee = {
    1: listmenuu().expresso,    2: listmenuu().americaSoda
  };
  Map tea = {
    1: listmenuu().chrysanthemum,    2: listmenuu().matchaLatte,
  };
  Map milkCocoaCaramel = {
    1: listmenuu().milk,    2: listmenuu().pinkMilk
  };
  Map proteinShake = {
    1: listmenuu().matchaProtein,    2: listmenuu().strawberryProtein,
  };
  Map soda = {
    1: listmenuu().icePepsi,    2: listmenuu().ice
  };
  

}